package org.nrg.xnat.generated.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "tip_plugin_benice", name = "XNAT 1.7 TIP Benice Plugin", description = "This is the XNAT 1.7 TIP Benice Plugin.",
        entityPackages="",
        dataModels = {
            @XnatDataModel(value = "tip:beniceQc", singular = "Benice", plural = "Benices")
        })
public class TipBenicePlugin {
}